//
// Projekt: Embedded Systems Workshop
// FH Wedel - Wintersemester 2017/2018
//
// @file    Sound.h
// @author  Anastasia Bublies
// @version 1.0
// @date    27.02.2018
// @brief   Wiedergabe eines Sound durch ein Piezoelement
//
// Initialisierung des Piezoelementes auf den GPIO Pin GPIO_PIN_12.
// Mit den Funktionen set_sound und reset_sound kann die
// Wiedergabe des Tons aktiviert bzw. deaktiviert werden.


#ifndef __SOUND_H_
#define __SOUND_H_

#include <stdio.h>
#include <stdlib.h>
#include <cmsis_device.h>
#include "timer.h"
#include "diag/Trace.h"

void sound_init(void);
void set_sound(void);
void reset_sound(void);
void sound_beep(void);


#endif
