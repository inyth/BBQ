//
// Projekt: Embedded Systems Workshop
// FH Wedel - Wintersemester 2017/2018
//
// @file    Display.h
// @author  Anastasia Bublies
// @version 1.0
// @date    27.02.2018
// @brief   Projektspeziefische Darstellung von von Texten
//          und Grafiken auf dem Touchdisplay.
//
// Dese Datei beinhaltet alle Textnachrichten und Grafiken f�r die
// Ausgabe auf dem Touchdisplay. Hier sind die Positionen der
// darzustellenden Elemete definiert.

#ifndef __DISPLAY_H_
#define __DISPLAY_H_

#include <stdio.h>
#include <stdlib.h>
#include <cmsis_device.h>
#include "diag/Trace.h"
#include <Evaluate.h>


void dis_draw_button(dis_button_struct_t button);
void dis_draw_left_arrow(dis_button_struct_t button);
void dis_draw_right_arrow(dis_button_struct_t button);

void dis_write_value(dis_value_t value);
void dis_write_all_values(status_t status, int sensor_kern, int sensor_ofen);

void dis_start_text(void);
void dis_run_text(int sensor_kern, int sensor_ofen, int TEMP_MIN, int TEMP_MAX);

void dis_time(int stunden, int minuten, int sekunden);
void dis_mute(int mute);


#endif
