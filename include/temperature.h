/*
 * temperature.h
 *
 *  Created on: 30.12.2017
 *      Author: larsj
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

/** Temperaturen und zugeh�rige Widerst�nde */
#define T0 295.15
#define R0 212839.50617283952
#define T1 313.15
#define R1 78000
#define T2 425.15
#define R2 1710

/** Referenzspannung */
#define Vref 3.3

/** Vorwiderstand */
#define Rpre 43000

void temperature_init(void);

/**
 * Liefert die Temperaturen zur�ck.
 */
double * temperature_get(void);

/**
 * Setzt die Grenzen f�r die zu �berwachenden Temperaturen.
 * Die ersten zwei Grenzen sind f�r die erste Temperatur, die letzten beiden Grenzen
 * f�r die zweite Temperatur.
 */
void temperature_setThresholds(double * _thresholds);

/**
 * Pr�ft, ob die Temperaturen die Grenzen verletzen.
 * Gibt den Index der zuerst gefundenen verletzten Grenze zur�ck.
 */
int temperature_checkThresholds(void);

//double * temperature_getDeltaTempPerSec(void);

#endif /* TEMPERATURE_H_ */
