//
// Projekt: Embedded Systems Workshop
// FH Wedel - Wintersemester 2017/2018
//
// @file    Evaluate.h
// @author  Anastasia Bublies
// @version 1.0
// @date    27.02.2018
// @brief   Funktionen zum Auswerten des Zustandsautomaten und der
//          Button-Callback-Funktionen
//
// Hier werden die Standardwerte gesetzt und die Callback-Funktionen der
// Buttons definiert, die �ber das Touchdisplay ausgel�st werden.
// Alle Funktinen zum aktualisieren des Zustandautomaten und des Displays
// sind hier definiert. Das periodische Lesen der Temperaturwerte findt
// ebenfalls hier statt.


#ifndef __EVALUATE_H_
#define __EVALUATE_H_

#include "LCD.h"
#include "Touch.h"


typedef enum {
    LEFT,
    RIGHT,
    NONE
} direction_e;

typedef struct {
    uint16_t xpos;
    uint16_t ypos;
    uint16_t value;
    uint16_t min;
    uint16_t max;
    uint8_t  status;
} dis_value_t;

typedef struct {
	uint16_t     xpos;
	uint16_t     ypos;
	uint16_t     width;
	uint16_t     height;
	uint16_t     color;
	uint8_t      status;
	direction_e  direction;
	dis_value_t* value;
} dis_button_struct_t;


typedef struct {
    dis_button_struct_t info;
    void (*callback)(dis_button_struct_t);
} dis_button_t;

//------------------------------------------------------

typedef enum {
    EVENT_NONE,
    EVENT_START_BUTTON,
    EVENT_TEMP_REACHED,
	EVENT_MUTE,
	EVENT_STOP,
	EVENT_MENUE,
}events;

extern events event;
//------------------------------------------------------

typedef enum {
	STARTMENUE,
	INITRUNMENUE,
	UPDATERUNMENUE,
}status_t;

#define TEXT_COLOR   BLACK
#define STATUS_SET   0
#define STATUS_RESET 1

extern dis_value_t sensor1_core;
extern dis_value_t sensor2_min;
extern dis_value_t sensor2_max;

extern int x_pos; // Position des Graphen (0/0)
extern int y_pos;
extern int x_len;
extern int y_len;

#define START_BUTTON_CNT 8
#define RUN_BUTTON_CNT 2
dis_button_t startmenue_button[START_BUTTON_CNT];
dis_button_t runmenue_button[RUN_BUTTON_CNT];

void dis_decremet(dis_button_struct_t button);
void dis_increment(dis_button_struct_t button);

void dis_start_button(dis_button_struct_t button);
void dis_mute_button(dis_button_struct_t button);
void dis_stop_button(dis_button_struct_t button);
void dis_menu_button(dis_button_struct_t button);

void update_time();
void update_sensor();

int norm_value(int temperatur);

extern void init_startmenue(void);
extern void draw_startmenue(void);
extern void update_startmenue(uint16_t hwXpos, uint16_t hwYpos);

extern void init_runmenue(void);
extern void draw_runmenue(void);
extern void update_runmenue(uint16_t hwXpos, uint16_t hwYpos);


#endif

