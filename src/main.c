//
// Projekt: Embedded Systems Workshop
// FH Wedel - Wintersemester 2017/2018
//
// @file    main.h
// @author  Anastasia Bublies, Lars Junker
// @version 1.0
// @date    27.02.2018
// @brief   Einstiegsfunktion
//
// Das Grillthermometer kann zwei Temperatursensoren lesen. Der erste Sensor
// misst die Kerntemperatur, der zweite die Temperatur des Grills oder Ofens.
// Das Anzeigen von Informationen sowie die Steuerung des Grillthermometers
// erfolgt �ber ein Touchdisplay. �ber das Touchdisplay ist es m�glich eine
// Ziel-Kerntemperatur und eine minimale und maximale Grilltemperatur �ber
// ein Men� festzulegen und anschlie�end die Messung zu starten. Anschlie�end
// erfolgt eine Ausgabe der Betriebszeit, die aktuellen Werte der Kern- und
// Grilltemperatur, sowie die Ziel-Kerntemperatur und die minimale und
// maximale Grilltemperatur.
//
// W�hrend des Messvorgangs wird die Kern- und Grilltemperatur gelesen und
// mit den Grenzwerten verglichen. Sobald eine Grenze �ber- bzw. unterschritten
// wird, wird ein visuellen Signal auf dem Display angezeigt und es ert�nt ein
// akustisches Signal. Bei einer zu schnellen Temperatur�nderung, erfolgt
// ebenfalls eine Ausgabe um auf einen zu schnellen Temperaturanstieg bzw.
// Temperaturabfall aufmerksam zu machen.

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "stm32f4xx.h"			// nucleo f401RE
#include <cmsis_device.h>
#include "LIB_Config.h"			// Grafikbibliothek f�r Touchdisplay
#include "PF_Config.h"
#include <Evaluate.h>			// Auswertung der Eingaben und Sensoren
#include "timer.h"				// Softwaretimer
#include "temperature.h"		// Temperatursensoren
#include "Sound.h"				// Piezoelement f�r Sound


// ----------------------------------------------------------------------------
//
// Standalone STM32F4 empty sample (trace via DEBUG).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the DEBUG output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


enum states {
  STATE_INIT,
  STATE_RUNNING,
  STATE_STOP,
  STATE_ERROR
};

enum states state = STATE_INIT;
enum states next_state = STATE_INIT;


int main(int argc, char* argv[]) {
	system_init();              		// System initalisieren
    timer_init();						// Softwaretimer initalisieren
    temperature_init();					// Temperatursensoren initalisieren
    sound_init();						// Piezoelement initalisieren
    xpt2046_init();             		// Touchdisplay initalisieren

    init_startmenue();          		// Initialisierung des Startmen�s
    draw_startmenue();          		// Zeichnen des Startmen�s

    timer_register(&update_sensor, 1);  // Regestrierung von Funktionen im Softwaretimer
    timer_register(&update_time,1);


    while (1) {
    	// Abfragen des Touchdisplays:
    	// X-/Y-Position des Druckpunktes auf dem Display oder
    	// -1/-1 wenn keine Eingabe stattgefunden hat
    	point_t pos = tp_get_xy();

        switch(state) {
        case STATE_INIT:
        	// Button auswerten
        	if (pos.hwXpos > 0 && pos.hwYpos > 0)
        		update_startmenue(pos.hwXpos, pos.hwYpos);

            if (event == EVENT_START_BUTTON) {
            	// Wenn Startbutton gedr�ckt wurde, in den Running-Modus wechseln
                next_state = STATE_RUNNING;
            }
            else if (event == EVENT_STOP) {
            	// Wenn Ende-Button gedr�ckt, in den Aus-Modus wechseln
            	next_state = STATE_STOP;
            }
            break;

        case STATE_RUNNING:
            if (event == EVENT_START_BUTTON) {
            	// Initialisierung des Running-Men�s wenn Start-Button gedr�ckt wurde
                init_runmenue();
                draw_runmenue();
                event = EVENT_NONE;
            }
            else if (event == EVENT_MENUE) {
            	// Start-Men�ansicht bei auswahl des Men�-Buttons.
            	// Auswertung wird im n�chsten Durchlauf fortgesetzt.
            	// Der Sound wird deaktiviert solange das Startmen� aktiv ist
            	init_startmenue();
                draw_startmenue();
                event = EVENT_NONE;
                next_state = STATE_INIT;
            }
            else {
            	if (pos.hwXpos > 0 && pos.hwYpos > 0)
            		// Aktualisieren des Runmen�s
            		update_runmenue(pos.hwXpos, pos.hwYpos);
            }
            break;

        case STATE_STOP:
        	// Im Aus-Modus wird der ausgeschaltete Zustand simuliert
        	reset_sound();
        	lcd_clear_screen(BLACK);
            event = EVENT_NONE;
            break;

        case STATE_ERROR:
        	// Fehlerbehandlungen hier m�glich
            break;
        }

        // den Zustand aktualisieren
        state = next_state;
    }

    /*
    // ----------------------------------------------------------------------------
    // Startet die Kalibrierungsfunktion f�r das Touchdisplay

    trace_printf("SYSTEM INIT\n");
    system_init();
    trace_printf("SYSTEM INIT DONE \n");

    trace_printf("LCD DISPLAY STRING 1 \n");
    lcd_display_string(60, 120, (const uint8_t *)"Hello, world !", FONT_1608, RED);
    trace_printf("LCD DISPLAY STRING 1 DONE \n");
    trace_printf("LCD DISPLAY STRING 2 \n");
    lcd_display_string(30, 152, (const uint8_t *)"2.8' TFT Touch Shield", FONT_1608, RED);
    trace_printf("LCD DISPLAY STRING 2 DONE \n");

    __SD_CS_SET();

    tp_adjust();
    tp_dialog();
    // ----------------------------------------------------------------------------
    */
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
