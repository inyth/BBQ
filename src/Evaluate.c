#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

#include "stm32f4xx.h"
#include <cmsis_device.h>
#include <assert.h>
#include <Evaluate.h>

#include "LIB_Config.h"
#include "PF_Config.h"
#include "Sound.h"
#include "Display.h"

#include "temperature.h"

#include <time.h>


int   CORE_MIN = 0;			// Min und Max Werte f�r die Kerntemparatur
int   CORE_MAX = 100;

float TEMP_MIN = 20;		// Min und Max Werte f�r die Ofentemperatur
float TEMP_MAX = 200;

int sensor_kern = 0;		// aktuelle Temperaturwerte des Sensoren
int sensor_ofen = 0;

dis_value_t sensor1_core;	// Ziel-Sensorwerte
dis_value_t sensor2_min;
dis_value_t sensor2_max;

// Events k�nnen durch das Dr�cken der Buttons auf dem Display ausgel�st werden
events event = EVENT_NONE;

int timecounter = 0;				// Zeit-Counter f�r die Graphendarstellung
int roundcounter = 0;				// Z�hler zur Halbierung der Zeit-Runden (Graph)
int roundno = 0;					// Anzahl Runden, indenen die Zeit halbiert wurde (Graph)
int temperatur_buffer_kern[276];	// Buffer f�r den Kern-Tepperatursensor (Graph)
int temperatur_buffer_ofen[276];	// Buffer f�r den Ofen-Temperatursensor (Graph)

int mute = 0;						// Mute-Zeit in Sekunden

int stunden = 0;					// aktuelle Betriebszeit
int minuten = 0;
int sekunden = 0;

int x_pos = 285;					// Null-Position des Graphen (0/0)
int y_pos = 230;
int x_len = 275;
int y_len = 170;

int first_init = 1;					// Initialisierungs-Flag


// CALLBACKFUNKTIONEN
//--------------------------------------------------------------------------------------------------------------------


// Callbackfunktion: Dektementiere den Ziel-Sensorwert
// (Auszuf�hren durch Benutzereingabe im Startmenue)
//
// @param  button dessen Wert aktualisiert werden soll
// @return ---
void dis_decremet(dis_button_struct_t button) {
    assert(button.value != NULL);

	if (button.value->value <= button.value->min) {
		// Soundausgabe bei Grenzwert�berschreitung
		sound_beep();
	}
    else {
    	button.value->value -= 1;
    	dis_write_value(*button.value);
    }
}


// Callbackfunktion: Incrementiere den Ziel-Sensorwert
// (Auszuf�hren durch Benutzereingabe im Startmenue)
//
// @param button dessen Wert aktualisiert werden soll
// @return ---
void dis_increment(dis_button_struct_t button){
    assert(button.value != NULL);

    if (button.value->value >= button.value->max) {
    	// Soundausgabe bei Grenzwert�berschreitung
    	sound_beep();
    }
    else {
    	button.value->value += 1;
        dis_write_value(*button.value);
    }
}


// Callbackfunktion: Aktiviere Event "Mit der Messung beginnen".
// Benutzereingabe START BUTTON im Startmenue aktiviert Runmenue.
//
// @param  button zum Starten der Messung
// @return ---
void dis_start_button(dis_button_struct_t button) {
	lcd_clear_screen(WHITE);
    event = EVENT_START_BUTTON;

    // �ber Button-Status kann die Messung fortgesetzt werden beim
    // zur�ckkehren ins Menu (z.B. um Temperaturwerte zu aktualisieren)
    startmenue_button[6].info.status = STATUS_SET;
}


// Callbackfuktion: Bei Grenzwert�berschreitung wird eine Soundausgabe
// aktiviert. Mit der Mutefunktion kann die Soundausgabe f�r 5 Minuten
// pausiert werden.
//
// @param  button aktiviert die Mute-Zeit
// @return ---
void dis_mute_button(dis_button_struct_t button) {
	mute = (60*5)+1;
	reset_sound();
}


// Callbackfuktion: Stoppt die Messung und schaltet das Display aus.
//
// @param  button l�st das Stop-Event aus
// @return ---
void dis_stop_button(dis_button_struct_t button) {
    event = EVENT_STOP;
}


// Callbackfuktion: W�hrend des Messvorgangs wird zur�ck in das
// Startmenu gewechselt. Die Messung wird fortgesetzt. Es ist jedoch
// m�glich die Ziel-Temperaturen im Startmen� anzpassen und die
// Messung anschlie�end fortzusetzen. Die Soundausgabe wird pausiert und
// die Mute-Zeit zur�ckgesetzt.
//
// @param  button aktiviert die Startmenue-Ansicht
// @return ---
void dis_menu_button(dis_button_struct_t button) {
	reset_sound();
	startmenue_button[6].info.status = STATUS_RESET;
	event = EVENT_MENUE;
}


// STARTMENUE
//--------------------------------------------------------------------------------------------------------------------

// Initialiesieren des Startmenue:
// Festlegen der Gr��e und Position der Buttons und
// Textausgaben (Ziel-Sensorwerte)
//
// @param  ---
// @return ---
void init_startmenue(void) {
	if (first_init) {
		// Position im Display festlegen.
		// Zielsonserwerte werden einmalig auf Startwerde gesetzt.
		// Beim Mehrfachen aufrufen (zur�ckkehren ind Startmen�)
		// behalten die Objekte ihre aktuellen Werte.

		// Values und Position der Sensorwertanzeige
		sensor1_core.status = 0;
		sensor1_core.value = 60;
		sensor1_core.xpos = 35;
		sensor1_core.ypos = 300;
		sensor1_core.min = CORE_MIN;
		sensor1_core.max = CORE_MAX;

		sensor2_min.status = 0;
		sensor2_min.value = 70;
		sensor2_min.xpos = 90;
		sensor2_min.ypos = 300;
		sensor2_min.min = TEMP_MIN;
		sensor2_min.max = TEMP_MAX;

		sensor2_max.status = 0;
		sensor2_max.value = 90;
		sensor2_max.xpos = 145;
		sensor2_max.ypos = 300;
		sensor2_max.min = TEMP_MIN;
		sensor2_max.max = TEMP_MAX;

		//-----------------------------------------------------

		// Gr��e und Position der Buttons festlegen
		startmenue_button[0].info.xpos = 10;
		startmenue_button[0].info.ypos = 85;
		startmenue_button[0].info.height = 70;
		startmenue_button[0].info.width = 50;
		startmenue_button[0].info.status = STATUS_SET;
		startmenue_button[0].info.direction = LEFT;
		startmenue_button[0].info.value = &sensor1_core;
		startmenue_button[0].callback = dis_decremet;

		startmenue_button[1].info.xpos = 65;
		startmenue_button[1].info.ypos = 85;
		startmenue_button[1].info.height = 70;
		startmenue_button[1].info.width = 50;
		startmenue_button[1].info.status = STATUS_SET;
		startmenue_button[1].info.direction = LEFT;
		startmenue_button[1].info.value = &sensor2_min;
		startmenue_button[1].callback = dis_decremet;

		startmenue_button[2].info.xpos = 120;
		startmenue_button[2].info.ypos = 85;
		startmenue_button[2].info.height = 70;
		startmenue_button[2].info.width = 50;
		startmenue_button[2].info.status = STATUS_SET;
		startmenue_button[2].info.direction = LEFT;
		startmenue_button[2].info.value = &sensor2_max;
		startmenue_button[2].callback = dis_decremet;

		startmenue_button[3].info.xpos = 10;
		startmenue_button[3].info.ypos = 10;
		startmenue_button[3].info.height = 70;
		startmenue_button[3].info.width = 50;
		startmenue_button[3].info.status = STATUS_SET;
		startmenue_button[3].info.direction = RIGHT;
		startmenue_button[3].info.value = &sensor1_core;
		startmenue_button[3].callback = dis_increment;

		startmenue_button[4].info.xpos = 65;
		startmenue_button[4].info.ypos = 10;
		startmenue_button[4].info.height = 70;
		startmenue_button[4].info.width = 50;
		startmenue_button[4].info.status = STATUS_SET;
		startmenue_button[4].info.direction = RIGHT;
		startmenue_button[4].info.value = &sensor2_min;
		startmenue_button[4].callback = dis_increment;

		startmenue_button[5].info.xpos = 120;
		startmenue_button[5].info.ypos = 10;
		startmenue_button[5].info.height = 70;
		startmenue_button[5].info.width = 50;
		startmenue_button[5].info.status = STATUS_SET;
		startmenue_button[5].info.direction = RIGHT;
		startmenue_button[5].info.value = &sensor2_max;
		startmenue_button[5].callback = dis_increment;

		startmenue_button[6].info.xpos = 175;
		startmenue_button[6].info.ypos = 10;
		startmenue_button[6].info.height = 145;
		startmenue_button[6].info.width = 55;
		startmenue_button[6].info.status = STATUS_RESET;
		startmenue_button[6].info.direction = NONE;
		startmenue_button[6].info.value = NULL;
		startmenue_button[6].callback = dis_start_button;

		startmenue_button[7].info.xpos = 175;
		startmenue_button[7].info.ypos = 165;
		startmenue_button[7].info.height = 145;
		startmenue_button[7].info.width = 55;
		startmenue_button[7].info.status = STATUS_SET;
		startmenue_button[7].info.direction = NONE;
		startmenue_button[7].info.value = NULL;
		startmenue_button[7].callback = dis_stop_button;

		first_init = 0;
	}
	else {
	    sensor1_core.xpos = 35;
	    sensor1_core.ypos = 300;

	    sensor2_min.xpos = 90;
	    sensor2_min.ypos = 300;

	    sensor2_max.xpos = 145;
	    sensor2_max.ypos = 300;
	}
}


// Zeichnen des Startmen�s auf dem Touchdisplay.
//
// @param  ---
// @return ---
void draw_startmenue(void) {
    lcd_clear_screen(WHITE);

    // zeichne alle Buttons
    for (int i = 0; i < START_BUTTON_CNT; i++)
        dis_draw_button(startmenue_button[i].info);

    // schreibt den Displaytext
    dis_start_text();

    // schreibt die Sensorwerte
    dis_write_all_values(STARTMENUE, sensor_kern, sensor_ofen);
}


// Auswerten der Position der Eingabe auf dem Touchdisplay.
// Es werden alle Buttons abgefragt. Sobald ein Button gedr�ckt wurde,
// wird die Collbackfunktion der Button-Struktur ausgef�hrt.
//
// @param  hwXposist die X-Position der Eingabe auf dem Touchisplay
// @param  hwYpos ist die Y-Position der Eingabe auf dem Touchdisplay
// @return ---
void update_startmenue(uint16_t hwXpos, uint16_t hwYpos) {
    for (int i = 0; i < START_BUTTON_CNT; i++) {
        if (hwXpos > startmenue_button[i].info.xpos && hwXpos < startmenue_button[i].info.xpos+startmenue_button[i].info.width
        &&  hwYpos > startmenue_button[i].info.ypos && hwYpos < startmenue_button[i].info.ypos+startmenue_button[i].info.height) {

        	// Testausgabe: Es wird ein roter Punkt gezeichnet an der Stelle, wo das Touchdisplay aktiviert wurde
            //tp_draw_big_point(hwXpos, hwYpos, RED);

            // Ausf�hren der Callbackfunktion des Buttons
            if (startmenue_button[i].callback != NULL)
                startmenue_button[i].callback(startmenue_button[i].info);
        }
    }
}


// RUNMENUE
//--------------------------------------------------------------------------------------------------------------------

// Initialisiere das Run-Menue:
// Festlegen der Position und Values der Textausgaben und Buttons
//
// @param  ---
// @return ---
void init_runmenue(void) {
	// Mute-Zeit zur�cksetzen
	mute = 0;

	// Temperaturwerte lesen
	double * temperature = temperature_get();
	sensor_kern = (int)temperature[0];
	sensor_ofen = (int)temperature[1];

	// Position der Temperaturausgabe im Display
    sensor1_core.xpos = 10;
    sensor1_core.ypos = 185;
    sensor2_min.xpos = 30;
    sensor2_min.ypos = 222;
    sensor2_max.xpos = 30;
    sensor2_max.ypos = 60;

    // Gr��e und Position der Buttons festlegen
    runmenue_button[0].info.xpos = 10;
    runmenue_button[0].info.ypos = 10;
    runmenue_button[0].info.height = 65;
    runmenue_button[0].info.width = 40;
    runmenue_button[0].info.status = STATUS_SET;
    runmenue_button[0].info.direction = NONE;
    runmenue_button[0].info.value = NULL;
    runmenue_button[0].callback = dis_mute_button;

    runmenue_button[1].info.xpos = 10;
    runmenue_button[1].info.ypos = 85;
    runmenue_button[1].info.height = 65;
    runmenue_button[1].info.width = 40;
    runmenue_button[1].info.status = STATUS_SET;
    runmenue_button[1].info.direction = NONE;
    runmenue_button[1].info.value = NULL;
    runmenue_button[1].callback = dis_menu_button;
}


// Zeichnen das Run-Menue (Buttons und Text) auf dem Display.
//
// @param  ---
// @raturn ---
void draw_runmenue(void) {
    lcd_clear_screen(WHITE);

    dis_draw_button(runmenue_button[0].info);
    dis_draw_button(runmenue_button[1].info);

    dis_run_text(sensor_kern, sensor_ofen, (int)TEMP_MIN, (int)TEMP_MAX);
}


// Lese und auswerten der aktuellen Temperaturwerte.
// - Wenn die Temperaturwerte die Grenzwerte (Graph) �berschreiten,
//   werden diese auf die Maximal- bzw. Minimalwerte zur�ckgesetzt.
// - Sound-Aktivierung bei �berschreitung der Ziel- bzw. Grenzwerte.
// - Anzeigen der Werte im Graphen
//
// @param  ---
// @return ---
void update_sensor() {
	if (startmenue_button[6].info.status == STATUS_SET) {

		double * temperature = temperature_get();

		sensor_kern = (int)temperature[0];
		sensor_ofen = (int)temperature[1];


		if ((mute <= 0) && ((sensor_kern >= sensor1_core.value)
						 || (sensor_ofen <= sensor2_min.value)
						 || (sensor_ofen >= sensor2_max.value))) {
			// Sound anmachen
			set_sound();
			event = EVENT_TEMP_REACHED;
		}

		// Grenzwerte f�r den Graphen setzen f�r die beiden Sensorwerte
		// Kerntemparatur und Ofentemperatue
		if (sensor_kern < TEMP_MIN) {
			sensor_kern = TEMP_MIN;
		}
		else if (sensor_kern > TEMP_MAX) {
			sensor_kern = TEMP_MAX;
		}

		if (sensor_ofen < TEMP_MIN) {
			sensor_ofen = TEMP_MIN;
		}
		else if (sensor_ofen > TEMP_MAX) {
			sensor_ofen = TEMP_MAX;
		}

		// Anzeigen aller Sensorwerte auf dem Display
		dis_write_all_values(UPDATERUNMENUE, sensor_kern, sensor_ofen);


		// Normierung der Sensorwete auf die Achsen im Displaygraphen
		int sensor_kern_n = norm_value(sensor_kern);
		int sensor_ofen_n = norm_value(sensor_ofen);


		if (timecounter >= x_len)
		{
			// Zeit l�uft ab jetzt halb so schnell
			timecounter = (int)(x_len/2);
			roundno++;

			// l�sche alte Werte im Graph (Display)
			for (int i = 0; i<x_len; i++) {
				lcd_draw_point(y_pos-temperatur_buffer_kern[i], x_pos-i-1, WHITE);
				lcd_draw_point(y_pos-temperatur_buffer_ofen[i], x_pos-i-1, WHITE);
			}

			// Achsen neu zeichnen
			lcd_draw_line(y_pos, x_pos, y_pos, x_pos-x_len, TEXT_COLOR);
			lcd_draw_line(y_pos, x_pos, y_pos-y_len, x_pos, TEXT_COLOR);


			// Alle aufgezeichneten Temperaturwerte auf die halbe Gr��e im Buffer zusammenfassen
			// und im Grapfen zusammengefasst anzeigen
			temperatur_buffer_kern[0] = (int)((temperatur_buffer_kern[0] + temperatur_buffer_kern[+1]) / 2);
			temperatur_buffer_ofen[0] = (int)((temperatur_buffer_ofen[0] + temperatur_buffer_ofen[+1]) / 2);
			lcd_draw_point(y_pos-temperatur_buffer_kern[0], x_pos, RED);
			for (int i = 1; i<timecounter; i++)
			{
				temperatur_buffer_kern[i] = (int)((temperatur_buffer_kern[2*i] + temperatur_buffer_kern[(2*i)+1]) / 2);
				temperatur_buffer_ofen[i] = (int)((temperatur_buffer_ofen[2*i] + temperatur_buffer_ofen[(2*i)+1]) / 2);
			}
		}

		for (int i = 1; i<timecounter; i++)
		{
			lcd_draw_point(y_pos-temperatur_buffer_kern[i], x_pos-i-1, RED);
			lcd_draw_point(y_pos-temperatur_buffer_ofen[i], x_pos-i-1, TEXT_COLOR);
		}

		if (roundcounter >= roundno) {
			roundcounter = 0;

			// aktuelle Temperatur im Buffer sichern
			temperatur_buffer_kern[timecounter] = (int)sensor_kern_n;
			temperatur_buffer_ofen[timecounter] = (int)sensor_ofen_n;

			// aktuelle Temperatur im Graphen anzeigen
			lcd_draw_point(y_pos-(int)sensor_kern_n, x_pos-timecounter-1, RED);
			lcd_draw_point(y_pos-(int)sensor_ofen_n, x_pos-timecounter-1, TEXT_COLOR);


			// Zeit erh�hen
			timecounter++;
		}

		roundcounter++;

	}

}


// Es wird gepr�ft, ob ein Button gedr�ckt wurde und die entsprechende
// Callback-Funktion aufgerufen.
//
// @param  hwXposist die X-Position der Eingabe auf dem Touchisplay
// @param  hwYpos ist die Y-Position der Eingabe auf dem Touchdisplay
// @return --
void update_runmenue(uint16_t hwXpos, uint16_t hwYpos) {
	if (startmenue_button[6].info.status == STATUS_SET) {
		for (int i = 0; i < RUN_BUTTON_CNT; i++) {
			if (hwXpos > runmenue_button[i].info.xpos && hwXpos < runmenue_button[i].info.xpos+runmenue_button[i].info.width
					&&  hwYpos > runmenue_button[i].info.ypos && hwYpos < runmenue_button[i].info.ypos+runmenue_button[i].info.height) {

				// Testausgabe: Es wird ein roter Punkt gezeichnet an der Stelle, wo das Touchdisplay aktiviert wurde
				//tp_draw_big_point(hwXpos, hwYpos, RED);

				// Ausf�hren der Callbackfunktion des Buttons
				if (runmenue_button[i].callback != NULL) {
					runmenue_button[i].info.status = STATUS_SET;
					runmenue_button[i].callback(runmenue_button[i].info);
				}
			}
		}
	}
}


// Es wird die Betriebszeit erh�ht und die Mute-Zeit tuntergez�hlt.
// Diese Funktion soll ein mal in der Sekunde aufregufen werden.
//
// @param  ---
// @return ---
void update_time() {
	// Incrementiere sekunde. Beim �berschreiten des Grenzwertes (59 Sekunden)
	// wird minute incrementiert. Genauso bei minuten und stunden.
	if (sekunden == 59) {
		sekunden = 0;
		//minuten++;

		if (minuten == 59) {
			minuten = 0;
			stunden++;
		}
		else {
			minuten++;
		}

	}
	else {
		sekunden++;
	}



	// decrementieren der Mute-Zeit
	if (mute >= 0)
		mute--;

	// aktualisieren der Betriebszeit und der Mute-Zeit, wenn im Run-Men�
	if (startmenue_button[6].info.status == STATUS_SET) {
		dis_time(stunden, minuten, sekunden);
		dis_mute(mute);
	}
}


// Normieren der Temperaturwerte auf die Graph-Skala (y-Achse).
//
// @param  temperatur ist der zu normierende Wert
// @return ist der normierte Wert
int norm_value(int temperatur) {
	return (int)(temperatur-TEMP_MIN)*(y_len/(TEMP_MAX-TEMP_MIN));
}


