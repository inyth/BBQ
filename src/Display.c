#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

#include "stm32f4xx.h"
#include <cmsis_device.h>
#include <assert.h>
#include <Evaluate.h>

#include "LIB_Config.h"
#include "PF_Config.h"
#include "Sound.h"
#include "Display.h"

#include "temperature.h"


#include <time.h>

// Hilfsfunktion zum Zeichnen eines Buttons auf dem Touchdisplay
//
// @param  button ist der zu zeichnende Button
// @return ---
void dis_draw_button(dis_button_struct_t button) {
	// Zeichne ein Viereck mit den Button-Eigenschaften
    lcd_draw_rect(button.xpos, button.ypos, button.width, button.height, TEXT_COLOR);

    // Pfeil auf dem Button zeichnen, wenn Flag gesetzt
    if(button.direction == LEFT)
        dis_draw_left_arrow(button);
    else if(button.direction == RIGHT)
        dis_draw_right_arrow(button);
}


// Hilfsfunktion zum Zeichnen eines Pfeils (links) auf dem Button
//
// param   button ist der Button indem ein Pfeil gezeichnet werden soll
// @return ---
void dis_draw_left_arrow(dis_button_struct_t button){
    lcd_draw_line(button.xpos+25, button.ypos+50, button.xpos+10, button.ypos+20, TEXT_COLOR);
    lcd_draw_line(button.xpos+25, button.ypos+50, button.xpos+40, button.ypos+20, TEXT_COLOR);
}


// Hilfsfunktion zum Zeichnen eines Pfeils (rechte) auf dem Button
//
// param   button ist der Button indem ein Pfeil gezeichnet werden soll
// @return ---
void dis_draw_right_arrow(dis_button_struct_t button){
    lcd_draw_line(button.xpos+25, button.ypos+20, button.xpos+10, button.ypos+50, TEXT_COLOR);
    lcd_draw_line(button.xpos+25, button.ypos+20, button.xpos+40, button.ypos+50, TEXT_COLOR);
}


// Aktualisiert den Sensorwert (Temperatur) auf dem Display
//
// @param  value ist eine Struktur, die den Sensorwert und Position beinhaltet
// @raturn ---
void dis_write_value(dis_value_t value) {
    // L�sche den alten Wert
    lcd_fill_rect(value.xpos, value.ypos-25, 20, 30, WHITE);

    // Schreibe den neuen Wert an Position xpos, ypos
    char buffer[4];
    sprintf(buffer, "%d'", value.value);
    lcd_display_string(value.xpos, value.ypos, (const uint8_t *)buffer, FONT_1608, TEXT_COLOR);
}


// Aktualisiert alle Ziel-Sensorwerte auf dem Touchdisplay
//
// @param  status gibt an in welchem Zustand sich das Programm befindet:
//         STARTMENUE, INITRUNMENUE oder UPDATERUNMENUE
// @param  sensor_kern ist die aktuelle Kerntemperatur
// @param  sensor_ofen ist die aktuelle Ofentemperatur
// @return ---
void dis_write_all_values(status_t status, int sensor_kern, int sensor_ofen) {
    char buffer[10];

    if (status == STARTMENUE) {
    	// Temperaturwerte im Startmenue zeichnen
    	sprintf(buffer, "%d'", sensor1_core.value);
    	lcd_display_string(sensor1_core.xpos, sensor1_core.ypos, (const uint8_t *) buffer, FONT_1608, TEXT_COLOR);

    	sprintf(buffer, "%d'", sensor2_min.value);
    	lcd_display_string(sensor2_min.xpos, sensor2_min.ypos, (const uint8_t *)buffer, FONT_1608, TEXT_COLOR);

    	sprintf(buffer, "%d'", sensor2_max.value);
    	lcd_display_string(sensor2_max.xpos, sensor2_max.ypos, (const uint8_t *)buffer, FONT_1608, TEXT_COLOR);
    }

    else if (status == INITRUNMENUE) {
    	// Initiale Temperaturwerte im Runmenue schreiben

        lcd_display_string(10, 310, (const uint8_t *) "KERN     | soll", FONT_1608, TEXT_COLOR);
        lcd_display_string(30, 310, (const uint8_t *) "OFEN     | ", FONT_1608, TEXT_COLOR);

        // schreibt die Ist-Sensorwerte
        sprintf(buffer, "%d", sensor_kern);
        strcat(buffer, "'");
        lcd_display_string(10, 270, (const uint8_t *) buffer, FONT_1608, TEXT_COLOR);

        sprintf(buffer, "%d", sensor_ofen);
        strcat(buffer, "'");
        lcd_display_string(30, 270, (const uint8_t *) buffer, FONT_1608, TEXT_COLOR);

        // schreibt die Ziel-Sensorwerte
        sprintf(buffer, "%d", sensor1_core.value);
        strcat(buffer, "'");
        lcd_display_string(sensor1_core.xpos, sensor1_core.ypos, (const uint8_t *) buffer, FONT_1608, TEXT_COLOR);

        sprintf(buffer, "%d", sensor2_min.value);
        strcat(buffer, "'/ ");
        char buffer2[10];
        sprintf(buffer2, "%d", sensor2_max.value);
        strcat(buffer, buffer2);
        strcat(buffer, "'");
        lcd_display_string(sensor2_min.xpos, sensor2_min.ypos, (const uint8_t *)buffer, FONT_1608, TEXT_COLOR);
    }

    else if (status == UPDATERUNMENUE) {
    	// Aktualisiere die Sensorwerte im Runmenue

    	lcd_fill_rect(10, 235, 40, 40, WHITE);

    	// schreibt die Ist-Sensorwerte
    	sprintf(buffer, "%d", sensor_kern);
    	strcat(buffer, "'");
    	lcd_display_string(10, 270, (const uint8_t *) buffer, FONT_1608, TEXT_COLOR);

    	sprintf(buffer, "%d", sensor_ofen);
    	strcat(buffer, "'");
    	lcd_display_string(30, 270, (const uint8_t *) buffer, FONT_1608, TEXT_COLOR);
    }
}


// Hilfsfunktion zum Anzeigen von Text im Startmenu
//
// @param  ---
// @return ---
void dis_start_text() {
	// schreibt den Displaytext
	lcd_display_string(15, 300, (const uint8_t *) "Kerntemperatur", FONT_1608, TEXT_COLOR);
	lcd_display_string(70, 300, (const uint8_t *) "Ofen min", FONT_1608, TEXT_COLOR);
	lcd_display_string(125, 300, (const uint8_t *) "Ofen max", FONT_1608, TEXT_COLOR);
	lcd_display_string(190, 100, (const uint8_t *) "START", FONT_1608, TEXT_COLOR);
	lcd_display_string(190, 250, (const uint8_t *) "AUS", FONT_1608, TEXT_COLOR);
}


// Hilfsfunktion zum Anzeigen von Text und den Temperaturen im Runmenue
//
// @param  sensor_kern ist die aktuelle Kerntemperatur
// @param  sensor_ofen ist die aktuelle Ofentemperatur
// @param  TEMP_MIN ist der Min-Wert f�r die y-Achse
// @param  TEMP_MAX ist der Max-Wert f�r die y-Achse
// @return ---
void dis_run_text(int sensor_kern, int sensor_ofen, int TEMP_MIN, int TEMP_MAX) {
	char buffer[8];

	// Butten-Text
    lcd_display_string(20, 61, (const uint8_t *) "LEISE", FONT_1608, TEXT_COLOR);
    lcd_display_string(20, 136, (const uint8_t *) "MENUE", FONT_1608, TEXT_COLOR);

    dis_write_all_values(INITRUNMENUE, sensor_kern, sensor_ofen);

    // x-/y-Achsen des Grafen
    lcd_draw_line(y_pos, x_pos, y_pos, x_pos-x_len, TEXT_COLOR);
    lcd_draw_line(y_pos, x_pos, y_pos-y_len, x_pos, TEXT_COLOR);

    // Zeichen Hilfslinien (Grenzwerte) auf dem Grafen
	lcd_draw_line(y_pos-norm_value(sensor2_min.value), x_pos-1, y_pos-norm_value(sensor2_min.value), x_pos-x_len-1, YELLOW);
	lcd_draw_line(y_pos-norm_value(sensor2_max.value), x_pos-1, y_pos-norm_value(sensor2_max.value), x_pos-x_len-1, YELLOW);
	lcd_draw_line(y_pos-norm_value(sensor1_core.value), x_pos-1, y_pos-norm_value(sensor1_core.value), x_pos-x_len-1, GREEN);

	// Achsenbeschriftung im Grafen
	sprintf(buffer, "%d'", TEMP_MIN);
	lcd_display_string(y_pos-15, x_pos+30, (const uint8_t *)buffer, FONT_1608, TEXT_COLOR);

	sprintf(buffer, "%d'", TEMP_MAX);
	lcd_display_string(y_pos-y_len, x_pos+30, (const uint8_t *)buffer, FONT_1608, TEXT_COLOR);
}


// Hilfsfunktion zum Anzeigen der Betriebszeit
//
// @param  stunden sind die Betriebsstunden
// @param  minuten sind die Betriebsminuten
// @param  sekunden sind die Betriebssekunden
// @return ---
void dis_time(int stunden, int minuten, int sekunden) {
	char zeit_buffer[8];

	lcd_fill_rect(y_pos-20, 10, 20, 80, WHITE);
	sprintf(zeit_buffer, "%02d:%02d:%02d", stunden, minuten, sekunden);
	lcd_display_string(y_pos-20, 75, (const uint8_t *)zeit_buffer, FONT_1608, TEXT_COLOR);
}


// Hilfsfunktion zum Anzeigen der Mute-Zeit
//
// @param  mute ist die verbleibende Mute-Zeit in Sekunden
// @return ---
void dis_mute(int mute) {
	if (mute >= 0) {
		// Zeit wird nur angezeigt, wenn Mute aktiv ist
		char mute_buffer[8];
		lcd_fill_rect(20, 20, 20, 50, WHITE);
		sprintf(mute_buffer, "%02d:%02d", (int)(mute / 60), (int)(mute % 60));
		lcd_display_string(20, 61, (const uint8_t *)mute_buffer, FONT_1608, TEXT_COLOR);

		if (mute <= 0) {
		// Wenn Mute-Zeit abgelaufen ist, wird die Buttonbeschriftung zur�ckgesetzt
			lcd_fill_rect(20, 20, 20, 50, WHITE);
			lcd_display_string(20, 61, (const uint8_t *) "LEISE", FONT_1608, TEXT_COLOR);
		}
	}
}





