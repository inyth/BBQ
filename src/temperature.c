/*
 * temperature.c
 *
 *  Created on: 30.12.2017
 *      Author: larsj
 */

#include "temperature.h"
#include "diag/Trace.h"
#include "stm32f4xx.h"
#include <math.h>

ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;
double beta = 0.0; //Materialkonstante
double Rinf = 0.0;
uint32_t ADC_Raw[2], Buffer[2];
double t[2];
double thresholds[4];

static void DMA_Init(void);
static void ADC_Init(void);

double temperature_calc(uint32_t analogRead);

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void _Error_Handler(char * file, int line) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	trace_printf("%s, %d\n", file, line);
	/* USER CODE END Error_Handler_Debug */
}

/* INIT_FUNKTIONEN */

void temperature_init(void) {
	for (int i = 0; i < 4; i++)
		thresholds[i] = 0.0;

	beta = (log(R1 / R2)) / ((1 / T1) - (1 / T2));
	Rinf = R0 * exp(-1.0 * beta / T0);

	DMA_Init();
	ADC_Init();

	HAL_ADC_Start_DMA(&hadc1, Buffer, 2);
}

/**
 * Initialisiert den ADC und seine Kan�le.
 */
static void ADC_Init(void) {
	ADC_ChannelConfTypeDef sConfig;

	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV4;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.ScanConvMode = ENABLE;
	hadc1.Init.ContinuousConvMode = ENABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 2;
	hadc1.Init.DMAContinuousRequests = ENABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&hadc1) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	sConfig.Channel = ADC_CHANNEL_0;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	sConfig.Channel = ADC_CHANNEL_1;
	sConfig.Rank = 2;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}
}

/**
 * Initialisiert den DMA.
 */
static void DMA_Init(void) {
	__HAL_RCC_DMA2_CLK_ENABLE()
	;

	HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
}

/* HANDLER_FUNKTIONEN */

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef * hadc) {
	switch (hadc->ErrorCode) {
	case HAL_ADC_ERROR_INTERNAL:
		trace_printf("ADC_ERROR!!! Internal Error");
		break; /*!< Internal error occurrence */
	case HAL_ADC_ERROR_DMA:
		trace_printf("ADC_ERROR!!! DMA Error");
		break; /*!< DMA error occurrence */
	case HAL_ADC_ERROR_OVR:
		trace_printf("ADC_ERROR!!! Overrun Error");
		break;
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef * hadc) {
	ADC_Raw[0] = Buffer[0];
	ADC_Raw[1] = Buffer[1];
}

void DMA2_Stream0_IRQHandler() {
	HAL_DMA_IRQHandler(&hdma_adc1);
}

/* API_FUNKTIONEN */

/**
 * Hilfsfunktion, um aus gemessener Spannung Temperatur zu berechnen.
 */
double temperature_calc(uint32_t analogRead) {
	double Vntc = Vref * (double) analogRead / 4096.0;
	double Vpre = Vref - Vntc;
	double I = Vpre / Rpre;
	//Widerstand am Thermometer
	double Rntc = Vntc / I;

	double TempK = (beta / log(Rntc / Rinf));
	double TempC = TempK - 273.15;
	return TempC;
}

double * temperature_get(void) {
	t[0] = temperature_calc(ADC_Raw[0]);
	t[1] = temperature_calc(ADC_Raw[1]);
	return t;
}

void temperature_setThresholds(double * _thresholds) {
	for (int i = 0; i < 4; i++)
		thresholds[i] = _thresholds[i];
}

int temperature_checkThresholds(void) {
	if (t[0] < thresholds[0])
		return 0;
	if (t[0] > thresholds[1])
		return 1;
	if (t[1] < thresholds[2])
		return 2;
	if (t[1] > thresholds[3])
		return 3;
	return -1;
}

//double * temperature_getDeltaTempPerSec(void) {
//	double tempPerSec[2];
//	double sec = deltaTime / 1000.0;
//
//	tempPerSec[0] = (t[0]-tOld[0]) / sec;
//	tempPerSec[1] = (t[1]-tOld[1]) / sec;
//
//	return tempPerSec;
//}
