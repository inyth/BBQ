/********************************************************************************
  * @file    LIB_Config.c 
  * @author  Waveshare Team
  * @version 
  * @date    13-October-2014
  * @brief     This files provide
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, WAVESHARE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LIB_Config.h"
#include <stdio.h>
#include <stdlib.h>

//Platform Configuration


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
//uint16_t g_hwDevId = 0;

SPI_HandleTypeDef tSPI;

/* Private function prototypes -----------------------------------------------*/
void system_init(void);
static void device_init(void);
static void driver_init(void);
static void port_init(void);
void spi1_init(void);

/**
  * @brief  System initialization.
  * @param  None
  * @retval  None
  */
void system_init(void)
{
	HAL_Init();

    device_init();
    driver_init();
}

static void device_init(void)	
{
	port_init();
	spi1_init();
}


static void driver_init(void)
{
	lcd_init();
	tp_init();

	/*HAL_Delay(1000);
	lcd_clear_screen(RED);
	HAL_Delay(1000);
	lcd_clear_screen(GREEN);
	HAL_Delay(1000);
	lcd_clear_screen(BLUE);
	HAL_Delay(1000);
    lcd_clear_screen(WHITE);*/
}

static void port_init(void) 
{
	GPIO_InitTypeDef tGPIO;

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();

	/*----------------------------------------------------------------------------------*/
	//SPI
	tGPIO.Pin = LCD_CLK_PIN | LCD_SDI_PIN | LCD_SDO_PIN;	 //SCK	MOSI  MISO
	tGPIO.Mode = GPIO_MODE_AF_PP;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &tGPIO);

	/*----------------------------------------------------------------------------------*/
	//LCD
	tGPIO.Pin = LCD_CS_PIN;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(LCD_CS_GPIO, &tGPIO);

	tGPIO.Pin = LCD_DC_PIN;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(LCD_DC_GPIO, &tGPIO);

	tGPIO.Pin = LCD_BKL_PIN;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(LCD_BKL_GPIO, &tGPIO);

	/*----------------------------------------------------------------------------------*/
	//XPT2046
	tGPIO.Pin = XPT2046_CS_PIN | SD_CS_PIN;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOB, &tGPIO);
	__SD_CS_SET();

	tGPIO.Pin = XPT2046_IRQ_PIN;
	tGPIO.Mode = GPIO_MODE_INPUT;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(XPT2046_IRQ_GPIO, &tGPIO);

	/*----------------------------------------------------------------------------------*/
	//SD
	tGPIO.Pin = XPT2046_CS_PIN | SD_CS_PIN;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;
	tGPIO.Pull = GPIO_NOPULL;
	tGPIO.Speed = GPIO_SPEED_FAST;
	tGPIO.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOB, &tGPIO);
	__XPT2046_CS_SET();

	/*--------------------------------------------------------------------------------*/
}

/**
  * @brief  SPI initialization.
  * @param  None
  * @retval None
  */
void spi1_init(void)
{
	__SPI1_CLK_ENABLE();

	/* Configure SPI */
	tSPI.Instance = SPI1;
	tSPI.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8; //128; //8;
	tSPI.Init.CLKPhase = SPI_PHASE_2EDGE;
	tSPI.Init.CLKPolarity = SPI_POLARITY_HIGH;
	tSPI.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
	tSPI.Init.CRCPolynomial = 7;
	tSPI.Init.DataSize = SPI_DATASIZE_8BIT;
	tSPI.Init.Direction = SPI_DIRECTION_2LINES;
	tSPI.Init.FirstBit = SPI_FIRSTBIT_MSB;
	tSPI.Init.Mode = SPI_MODE_MASTER;
	tSPI.Init.NSS = SPI_NSS_SOFT;
	tSPI.Init.TIMode = SPI_TIMODE_DISABLED;

	HAL_SPI_Init((SPI_HandleTypeDef*)&tSPI);
	__HAL_SPI_ENABLE(&tSPI);

}


/*-------------------------------END OF FILE-------------------------------*/

