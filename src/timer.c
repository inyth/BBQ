#include "timer.h"
#include <stdlib.h>

/* List of tasks */
tasklist tasks;
/* Counter, how many tasks are already registered */
int reg_count;


/*
 * Initialize the timer module.
 * Must be called before any other usage of the module.
 * All previously registered timers are deleted!
 *
 * Return: 1 for success, 0 for failure
 */
int timer_init(void) {
	reg_count = 0;
	for (int i = 0; i < MAX_TIMERS; i++) {
		tasks[i].callback = NULL;
		tasks[i].counter = 0;
		tasks[i].div = 0;
	}
	return 1;
}


/*
 * Register a new timer function.
 * The new timer function "timerfkt" is called with a frequency of
 * "TICKRATE_HZ" * "div".
 * @param timerfkt Pointer to the timer function that is to be called
 * qparam div Clock divider f_out = f_in / div
 * Return: 1 for success, 0 for failure
 */
int timer_register(fptimerfkt timerfkt, uint32_t div) {
	if (reg_count >= MAX_TIMERS) {
		return 0;
	}
	tasks[reg_count].callback = timerfkt;
	tasks[reg_count].div = div;
	reg_count++;
	return 1;
}


/*
 * Process next tick.
 * Must be called periodically with a frequency of TICKRATE_HZ
 * Return: 1 for success, 0 for failure
 */
int timer_tick(void) {
	for (int i = 0; i < reg_count; i++) {
		tasks[i].counter++;
		if (tasks[i].div * TICKRATE_HZ == tasks[i].counter) {
			tasks[i].counter = 0;
			(*tasks[i].callback)();
		}
	}
}
