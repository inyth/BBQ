#include <stdio.h>
#include <stdlib.h>
#include <cmsis_device.h>
#include <Sound.h>
#include "timer.h"
#include "diag/Trace.h"


GPIO_InitTypeDef gpio;

int sound_on = 0;

// Initialisierung des Piezoelements
//
// @param  ---
// @return ---
void sound_init() {
 	// At this stage the system clock should have already been configured
	// at high speed.

	sound_on = 0;

	// PIN on output
	gpio.Alternate = 0;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;
	gpio.Pin = GPIO_PIN_12;
	gpio.Pull = GPIO_NOPULL;
	gpio.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOA, &gpio);
	
	HAL_GPIO_WritePin(GPIOA,gpio.Pin, GPIO_PIN_RESET);
}


// Soundausgabe aktivieren
//
// @param  ---
// @return ---
void set_sound() {
	sound_on = 1;
	HAL_GPIO_WritePin(GPIOA,gpio.Pin, GPIO_PIN_SET);
}

// Soundausgabe deaktivieren
//
// @param  ---
// @return ---
void reset_sound() {
	sound_on = 0;
	HAL_GPIO_WritePin(GPIOA,gpio.Pin, GPIO_PIN_RESET);
}

// Kurze Tonwiedergabe z.B. als Warnung bei ungültiger Eingabe.
//
// @param  ---
// @return ---
void sound_beep() {
	set_sound();
	HAL_Delay(100);
	reset_sound();
}


